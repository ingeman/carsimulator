﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSimulator.Sources
{
    public class Score
    {
        public int Valeur;

        public Score()
        {
            Valeur = 0;
        }

        public void Augmenter()
        {
            Valeur ++;
        }

    }
}
