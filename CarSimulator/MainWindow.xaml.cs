﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using CarSimulator.Sources;
using System.ComponentModel;

namespace CarSimulator
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Car car = null;
        private Score score;
        private Triangle triangle = null;
        private Timer timer = null;

        private const double MeterPixelsRatio = 1;

        private DateTime time;

        public MainWindow()
        {
            InitializeComponent();
            
            car = new Car();
            triangle = new Triangle(UITriangle);
            timer = new Timer(50);

            this.Start();
        }


        public void Start()
        {
            score = new Score();
            this.KeyDown += OnKeyDown;

            time = DateTime.Now;

            timer.Elapsed += timerElapsed;
            timer.AutoReset = true;
            timer.Start();
        }

        void timerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // UI THREAD
            this.Dispatcher.Invoke(new Action(delegate()
            {
                update(e.SignalTime - time);
                
                draw();

                time = e.SignalTime;
            }));
        }

        private void OnKeyDown(object sender, KeyEventArgs keyEvent)
        {
            if (keyEvent == null) return;
            
            switch (keyEvent.Key)
            {
                case Key.A:
                    car.Accelerate();
                    //car.updateDirection(Math.PI/2.0);
                    break;
                case Key.Z:
                    car.Decelerate();
                    //car.updateDirection(3*Math.PI / 4.0);
                    break;
                case Key.Up:
                    car.direction = Diretion.Haut;
                    break;
                case Key.Down:
                    car.direction = Diretion.Bas;
                    break;
                case Key.Left:
                    //car.updateDirection(Math.PI);
                    car.direction = Diretion.Gauche;
                    break;
                case Key.Right:
                    car.direction = Diretion.Droite;
                    //car.updateDirection(0.0);
                    break;
                case Key.C:
                    Random randonGen = new Random();
                    System.Drawing.Color randomColor = System.Drawing.Color.FromArgb(randonGen.Next(255), randonGen.Next(255), randonGen.Next(255) );
                    System.Windows.Media.Color newColor = System.Windows.Media.Color.FromArgb(randomColor.A, randomColor.R, randomColor.G, randomColor.B);
                    triangle.ChangeColor(newColor);
                    break;
            }
        }

        public void update(TimeSpan delay)
        {
            car.updatePosition(delay);
            clampPosition(car);
            TextBlockScore.Text = "score : " + score.Valeur;
        }

        public void draw()
        {
            triangle.draw(car.position);
        }

        public void clampPosition(Car car)
        {
            if (car.position.X < 0)
            {
                car.position.X = 0;
            }
            if(car.position.X >= this.Width - UITriangle.Width - 16)
            {
                car.position.X = this.Width - UITriangle.Width - 16;
            }


            if (car.position.Y < 0)
            {
                car.position.Y = 0;
            }
            if (car.position.Y >= this.Height - UITriangle.Height - 39)
            {
                car.position.Y = this.Height - UITriangle.Height - 39;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
