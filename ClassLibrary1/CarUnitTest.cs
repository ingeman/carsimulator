﻿using CarSimulator;
using CarSimulator.Sources;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CarSimulatorTestUnit
{
    [TestFixture]
    public class CarUnitTest
    {
        [Test]
        public void TestAvancement()
        {
            Car c = new Car();
            Assert.AreEqual(c.Avancement(Math.Acos(4 / 5), 5), new Point(4, 3));
        }

        [Test]
        public void TestChangementCouleur()
        {
            Triangle t = new Triangle();
            t.ChangeColor(Colors.AliceBlue);
            Assert.AreEqual(Colors.AliceBlue,t.Color);
        }

        [Test]
        public void TestAccelerate()
        {
            Car c = new Car();
            int speed = c.speed;

            for (int i = 1; i <= 100; i++)
            {
                c.Accelerate();
                var s = speed + 5 * i;
                if (s > 180)
                {
                    s = 180;
                }
                Assert.AreEqual(c.speed, s);
            }
        }

        [Test]
        public void TestDecelerate()
        {
            Car c = new Car();
            int speed = c.speed;

            for (int i = 1; i <= 100; i++)
            {
                c.Decelerate();
                var s = speed - 5 * i;
                if (s < 0)
                {
                    s = 0;
                }
                Assert.AreEqual(c.speed, s);
            }
        }

        [Test]
        public void TestScore()
        {
            Score score = new Score();

            Assert.AreEqual(score.Valeur,0);

            score.Augmenter();

            Assert.AreEqual(score.Valeur, 1);
        }
    }
}
