﻿using CarSimulator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Project1
{
    [TestFixture]
    public class CarUnitTest
    {
        [Test]
        public void TestAvancement()
        {
            Car c = new Car();
            Assert.AreEqual(c.Avancement(Math.Acos(4/5),5), new Point(4,3));
        }

        [Test]
        public void TestAvancementJuste()
        {
            Car c = new Car();
            Assert.AreEqual(new Point(4, 3), new Point(4, 3));
        }
    }
}
