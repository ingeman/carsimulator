﻿using System;
using System.Windows;

namespace CarSimulator
{
    public enum Diretion
    {
        None,
        Droite,
        Gauche,
        Haut,
        Bas
    }

    public class Car
    {
        public int speed;
        public Point position;

        public Diretion direction;

        public Car()
        {
            speed = 10;
            position = new Point();
        }

        public void updatePosition(TimeSpan delay)
        {
            switch (direction)
            {
                case Diretion.Droite:
                    position.X += speed * (delay.Milliseconds / 1000.0);
                    break;
                case Diretion.Gauche:
                    position.X -= speed * (delay.Milliseconds / 1000.0);
                    break;
                case Diretion.Haut:
                    position.Y -= speed * (delay.Milliseconds / 1000.0);
                    break;
                case Diretion.Bas:
                    position.Y += speed * (delay.Milliseconds / 1000.0);
                    break;
            }
            //position.Y += speed * delay.Seconds;
        }

        public void Accelerate()
        {
            if (speed < 180)
            {
                speed += 5;
            }   
        }

        public void Decelerate()
        {
            if (speed > 0)
            {
                speed -= 5;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="speed"></param>
        /// <returns></returns>
        public Point Avancement(Double direction, int speed)
        {
            return new Point();
        }

        //public void updateDirection(double direction)
        //{

        //}
        
        public void goLeft()
        {
            //this.direction += Math.PI/10.0;
        }

        public void goRight()
        {
            //this.direction -= Math.PI / 10.0;
        }
    }
}
