﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CarSimulator
{
    public class Triangle
    {
        public Color Color;
        public Vector Size;

        public Rectangle rectangle = null;
        public Triangle()
        {
            Color = Colors.Black;
        }

        public Triangle(Rectangle rectangle)
        {
            this.rectangle = rectangle;
            Color = ((SolidColorBrush)rectangle.Fill).Color;
        }

        public void ChangeColor(Color color)
        {
            Color = color;
        }

        public void draw(System.Windows.Point point)
        {
            rectangle.Fill = new SolidColorBrush(Color);


            TranslateTransform translate = new TranslateTransform(point.X,point.Y);



            /*TransformGroup transform_group = new TransformGroup();
            transform_group.Children.Add(translate);

            rectangle.RenderTransform = transform_group;*/

            rectangle.Margin = new Thickness(point.X, point.Y, 0, 0);
        }
    }
}
